import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class test {
    public WebDriver driver;
    WebElement cart;

    // settings
    String product = "iPhone";


    //open page "home"
    @Test(priority=1)
    void pageLoaded(){
        driver.get("https://demo.opencart.com/index.php?route=common/home");
        Wait wait = new WebDriverWait(driver, 2);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//a[text()='MP3 Players']")));
        WebElement title = driver.findElement(By.xpath("//a[text()='MP3 Players']"));
        boolean actual = title.isDisplayed();
        Assert.assertTrue(actual, "Page is not opened");
    }
    //add 1s product
    @Test(priority=2)
    void cartIsEmpty(){
        cart = driver.findElement(By.xpath("//div[@class='caption' and .//h4[.='"+product+"']]//following-sibling::div//span[.='Add to Cart']"));
        cart.click();
        Wait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='cart-total']")));
        WebElement text_cart = driver.findElement(By.xpath("//span[@id='cart-total']"));
        WebElement text_product = driver.findElement(By.xpath("//div[@class='caption' and .//h4[.='"+product+"']]//p[@class='price']"));
        String[] Text_product = (text_product.getText()).split("\n");
        System.out.println(Text_product[0]);
        boolean actual = text_cart.getText().equals("1 item(s) - "+Text_product[0]);
        Assert.assertTrue(actual, "Cart not add 1s product");
    }
    //open page product
    @Test(priority=3)
    void epenPageProduct() throws InterruptedException {
        cart = driver.findElement(By.xpath("//div[@class='caption' and .//h4[.='"+product+"']]//h4//a"));
        cart.click();
        Wait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//h1[text()='"+product+"']")));
        WebElement text = driver.findElement(By.xpath("//h1[text()='"+product+"']"));
        boolean actual = text.getText().equals(product);
        Assert.assertTrue(actual, "Page product is not opened");
    }


//пред тесты пройдены теперь тест кейс
    //add 2s product
    @Test(priority=4)
    void addTwoProductInCart() throws InterruptedException {
        cart = driver.findElement(By.xpath("//button[text()='Add to Cart']"));
        cart.click();
        Wait wait = new WebDriverWait(driver, 15);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//div[text()='Success: You have added ']")));
        WebElement textAlert = driver.findElement(By.xpath("//span[@id='cart-total']"));
        boolean actual = textAlert.isDisplayed();
        Assert.assertTrue(actual, "Alert not find!");
    }

    //check cart
    @Test(priority=5)
    void checkCart() throws InterruptedException {
        cart = driver.findElement(By.xpath("//button[@class='btn btn-inverse btn-block btn-lg dropdown-toggle']"));
        cart.click();
        Wait wait = new WebDriverWait(driver, 2);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td//a[text()='"+product+"']")));
        WebElement cart_ = driver.findElement(By.xpath("//td//a[text()='"+product+"']"));
        boolean actual_1 = cart_.isDisplayed();

        cart_ = driver.findElement(By.xpath("//table[@class='table table-striped']//td[text()='x 2']"));
        boolean actual_2 = cart_.getText().equals("x 2");

        boolean actual = false;
        if (actual_2 == true && actual_1 == true){
            actual = true;
        }


        Assert.assertTrue(actual, "Product is not 2!");
    }
//    @Test(priority=5)
//    void checkAdd() throws InterruptedException {
//        cart = driver.findElement(By.xpath("//button[@class='btn btn-inverse btn-block btn-lg dropdown-toggle']"));
//        cart.click();
//        Wait wait = new WebDriverWait(driver, 2);
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td//a[text()='"+product+"']")));
//        WebElement cart_ = driver.findElement(By.xpath("//td//a[text()='"+product+"']"));
//        boolean actual = cart_.isDisplayed();
//        Assert.assertTrue(actual, "Cart not find!");
//    }




//    //add 2s product
//    @Test(priority=3)
//    void cartOneItem() throws InterruptedException {
//        cart = driver.findElement(By.xpath("//div[@class='caption' and .//h4[.='iPhone']]//following-sibling::div//span[.='Add to Cart']"));
//        cart.click();
//        Wait wait = new WebDriverWait(driver, 15);
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//span[@id='cart-total']")));
//        WebElement text = driver.findElement(By.xpath("//span[@id='cart-total']"));
//        boolean actual = text.getText().equals("2 item(s) - $246.40");
//        Assert.assertTrue(actual, "Cart not add 2s product");
//    }




    //check 3
//    @Test(priority=4)
//    void cartOneItem() throws InterruptedException {
//        cart = driver.findElement(By.xpath("//div[@id='cart']"));
//        cart.click();
//        Wait wait = new WebDriverWait(driver, 1);
//        wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//td[@class='text-left']]")));
//        WebElement text = driver.findElement(By.xpath("//td[@class='text-left']"));
//
//        boolean actual = text.getText().equals("//td[@class='text-left']");
//
//
//
//        Assert.assertTrue(actual, "Cart not add 2s product");
//    }


    @BeforeClass
    void before(){
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
    }

    @AfterClass
    void postConditions(){
//        driver.quit();
    }
}
